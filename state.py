import hashlib

""" Store state """
class State(object):

    def __init__(self,namespace=None):

        self.namespace = 'default'
        if namespace:
            self.namespace = hashlib.sha224(namespace.encode('utf-8')).hexdigest()

        self.__data = {
                self.namespace: {}
        }

    def all(self):
        return {key: self.__data[self.namespace][key]['value'] for key  in self.__data[self.namespace].keys()}

    def __str__(self):
        return ("State(%s), %d objects" % (
            self.namespace, 
            len(self.__data[self.namespace])
        ))

    def exists(self, key):
        try:
            self.__data[self.namespace][key]
            return True
        except KeyError:
            return False

    """ Get values by attributes, returns string """
    def get(self, key):

        try:
            entry = self.__data[self.namespace][key]
            if(callable(entry['process'])):
                entry['process'](self, key, 'get')
            return entry['value']
        except KeyError:
            return ''

    def delete(self, key, process=None):
        entry = self.__data[self.namespace][key]
        if(callable(entry['process'])):
            entry['process'](self, key, 'del')
        del self.__data[self.namespace][key]
        return True

    def set(self, key, value, process=None):

        if not process:
            # Carry the process over?
            try:
                process = self.__data[self.namespace][key]['process']
            except KeyError:
                # No preexisting proces
                pass

        if(callable(process)):
            process(self, key, 'set')

        self.__data[self.namespace][key] = {
                'value': str(value),
                'process': process,
        }

        return value



        
