import unittest
import state
import processors

class VanilaTest(unittest.TestCase):

    def test_initialize(self):

        self.assertTrue(state.State())

    def test_set_value(self):

        s = state.State()
        self.assertEqual(s.set('k', 'v'), 'v')

    def test_get_value(self):
        s = state.State()
        s.set('k','v')
        self.assertEqual(s.get('k'), 'v')
    
    def test_namespace(self):
        s = state.State('loop')
        for i in range(0,123):
            s.set(i,'v')
        self.assertEqual(str(s), 'State(893f2cdf43fb6d36ba19c3e9a20c2c13aa27305d4bfaa741b26eb308), 123 objects')

    def test_del(self):
        s = state.State()
        s.set('k','v')
        self.assertTrue(s.delete('k'))

    def test_failed_del(self):
        s = state.State()
        with self.assertRaises(KeyError):
            s.delete('failure is always an option')

class ProcessorsTest(unittest.TestCase):

    def test_do_nothing(self):
        s = state.State()
        self.assertEqual(s.set('k', 'v',processors.do_nothing),'v')

    def test_get_reset(self):
        s = state.State()
        self.assertEqual(s.set('k', 'v',processors.get_reset),'v')
        self.assertEqual(s.get('k'), 'v')
        self.assertEqual(s.get('k'), '')

    def test_set_no_override(self):
        s = state.State()
        self.assertEqual(s.set('k', 'v',processors.set_no_override),'v')
        self.assertEqual(s.get('k'), 'v')
        with self.assertRaises(RuntimeError):
            s.set('k', 'v2')

    def test_del_protected(self):
        s = state.State()
        self.assertEqual(s.set('k', 'v',processors.del_protect),'v')
        self.assertEqual(s.set('k2', 'v2'),'v2')
        self.assertTrue(s.delete('k2'))
        with self.assertRaises(RuntimeError):
            self.assertTrue(s.delete('k'))

if __name__ == '__main__':
    unittest.main()
