
""" Do nothing"""
def do_nothing(cls, key, operation):
    pass

""" Get and remove"""
def get_reset(cls, key, operation):
    if operation == 'get':
        cls.set(key, '')

def set_no_override(cls, key, operation):

    if operation == 'set':
        if(cls.get(key) != ''):
            raise RuntimeError("Can't unset data")

def del_protect(cls, key, operation):

    if operation == 'del':
        if cls.exists(key):
            raise RuntimeError("Protected data")

