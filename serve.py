from flask import Flask, request, jsonify
from state import State
import processors

app = Flask(__name__)

instances = {}

@app.route('/state/<ns>')
def namespace(ns):
    if ns in instances:
        return jsonify({'instance': 'existing', 'data': instances[ns].all()})
    else:
        instances[ns] = State(ns)
        return jsonify({'instance': 'new'})

@app.route('/state/<ns>/<key>')
def value(ns, key):
    if ns in instances:
        return jsonify({key: instances[ns].get(key)})
    return jsonify({'error': 'Instance %s does not exists' % ns})

@app.route('/state/<ns>/<key>/<value>')
def set(ns, key, value):
    if ns in instances:
        instances[ns].set(key, value)
        return jsonify({key: instances[ns].get(key)})
    return jsonify({'error': 'Instance %s does not exists' % ns})

@app.route('/state/processors')
def processors():
    return jsonify({'processors': [s for s in dir(processors) if not s.startswith('__')]})

if __name__ == "__main__":
    app.run(debug=True)
